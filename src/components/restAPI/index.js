import React,{useState} from 'react'
import {useGet} from 'restful-react'

export const Afghan = () => {
const {data: randomDogImage, loading, refetch} = useGet({
    
    path: '/breed/hound/afghan/images/random'
})


    return (
        loading ? <h1>loading...</h1> :
        (<div>
            <div>
                <img src={randomDogImage.message} width="200px" height="200px"/>
            </div>
            <button onClick={()=> refetch()}>Losuj nowe zdjęcie pieska</button>
        </div>
        )
    )
}

export const Basset = () => {
    const {data: randomDogImage, loading, refetch} = useGet({
        path: `/breed/hound/basset/images/random`,
    })

        return (
            loading ? <h1>loading...</h1> :
            (<div>
                <div>
                    <img src={randomDogImage.message} width="200px" height="200px"/>
                    
                </div>
                <button onClick={()=> refetch()}>Losuj nowe zdjęcie pieska</button>
            </div>
            )
        )
    }

export const Blood = () => {
        const {data: randomDogImage, loading, refetch} = useGet({
            path: `/breed/hound/blood/images/random`
        })
        
            return (
                loading ? <h1>loading...</h1> :
                (<div>
                    <div>
                        <img src={randomDogImage.message} width="200px" height="200px"/>
                    </div>
                        <button onClick={()=> refetch()}>Losuj nowe zdjęcie pieska</button>
                </div>
                )
            )
        }

export const Plott = () => {
const {data: randomDogImage, loading, refetch} = useGet({
    path: `/breed/hound/plott/images/random`,
})


    return (
        loading ? <h1>loading...</h1> :
        (<div>
            <div>
                <img src={randomDogImage.message} width="200px" height="200px"/>
            </div>
                <button onClick={()=> refetch()}>Losuj nowe zdjęcie pieska</button>
        </div>
        )
    )
}

