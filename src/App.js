import './App.css';
// import Afghan from './components/afghan';
// import Basset from './components/basset';
// import Blood from './components/blood';
// import Plott from './components/plott';
import {Afghan,Basset,Blood,Plott} from './components/restAPI'


function App() {

  
  return (
    <div className="App">
      <Afghan/>
      <Basset/>
      <Blood/>
      <Plott/>
    </div>
  );
}

export default App;

